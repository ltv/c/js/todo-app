const todos = [];

/**
 *  Insert todo to database
 * 
 * @todo {id, title, completed} 
 */

exports.insert = todo => {
    const tobeTodo = { ...todo, completed: false };
    todos.push( tobeTodo );
    return tobeTodo;
}

exports.updateById = todo => {
    let todoIdx = todos.findIndex( t => t.id === todo.id ); // Todo || undefined
    if ( todoIdx != -1 ) {
        todos[todoIdx] = { ...todos[todoIdx], ... todo };
        return todos[todoIdx];
    } else {
        return false;
    }
};

exports.deletedById = id => {
    const todoIdx = todos.findIndex( todo => todo.id === id );
    if ( todoIdx === -1 ) {
        return false;
    }
    todos.splice(todoIdx, 1); 
    return true;
}

exports.findAll = params => {
    if (!params) {
        return todos;
    } else {
        const { completed } = params;
        return todos.filter( p => p.completed === completed );
    }
}

exports.todos = todos;
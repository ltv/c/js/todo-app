const express = require('express');
const router = express.Router();

const { createTodo, updateTodo, getTodoList, clearCompleted, deleteTodo } = require( '../controllers/todoController.js' );

router.post('/createTodo', createTodo);
router.post('/updateTodo', updateTodo);
router.post('/getTodoList', getTodoList);
router.post('/deleteTodo', deleteTodo);
router.post('/clearCompleted', clearCompleted);

module.exports = router;